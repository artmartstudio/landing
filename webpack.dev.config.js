var path = require("path");
var webpack = require("webpack");
var autoprefixer = require("autoprefixer");

process.noDeprecation = true;

var config = {
    entry: path.resolve(__dirname, "src/index.js"),
    output: {
        path: __dirname,
        filename: "dist/bundle.js",
        publicPath: "/"
    },
    devtool: "eval-source-map",
    devServer: {
		historyApiFallback: true,
        proxy: [
            {
                context: ['/api/**', '/images/**'],
                target: "http://0.0.0.0:1337/",
                secure: false
            }
        ]
    },
    module: {
        loaders: [{
                test: /\.(jsx|js)$/,
                loader: "babel-loader",
                exclude: /node_modules\/react-display-name/,
                query: {
                    presets: ["es2015", "react", "stage-0"],
                    plugins: ["transform-decorators-legacy"]
                }
            },

            {
                test: /\.(scss|sass)$/,
                loaders: ["style-loader", "css-loader", "postcss-loader", "sass-loader"]
            },

            {
                test: /\.(css)$/,
                loaders: ["style-loader", "css-loader", "postcss-loader"]
            },

            {
                test: /\.svg$/,
                include: [path.resolve(__dirname, "src", "images")],
                exclude: [path.resolve(__dirname, "src", "images", "static")],
                loader: "svg-inline-loader"
            },

            {
                test: /\.(svg|woff|woff2|ttf|png)$/,
                include: [
                    path.resolve(__dirname, "node_modules"),
                    path.resolve(__dirname, "src", "images", "static"),
                    path.resolve(__dirname, "src", "fonts")
                ],
                loader: "url-loader",
                query: {
                    limit: 100000
                }
            },

            {
                test: /\.json$/,
                loader: "json-loader"
            }
        ]
    },

    resolve: {
        modules: [path.resolve(__dirname, "src"), "node_modules"],
        extensions: [".js", ".jsx", ".scss", ".sass", ".json", ".css"]
    },
};

module.exports = config;