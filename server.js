import express from 'express'
import cookieParser from 'cookie-parser'
import bodyParser from 'body-parser'
import React from 'react'
import ReactDOMServer from 'react-dom/server'
import { Provider } from 'mobx-react'
import { match, RouterContext } from 'react-router'
import createMemoryHistory from 'history/lib/createMemoryHistory'
import Helmet from 'react-helmet'
import { getStore, createRoutes } from 'routes'
import template from './dist/index.tmpl.html'
import axios from 'axios'
import _ from 'lodash'
import { auth } from 'api'

import initialState from './src/models/MainStore'

import fs from 'fs'
import util from 'util'
import { dump } from 'nodedump'
var write = fs.writeFileSync
var file = "./props-obs.html"


class Server {
	constructor(port) {
		this.port = port
	}

	authentificateUser(token) {
		return new Promise((resolve, reject) => {
			console.log(token)
			if (token !== undefined) {
				auth.get_authCheck().then(resp => {
					if (resp.status === '200') {
						auth.get_userRole().then(response => {
							resolve(response.data)
						})
					} else {
						resolve(undefined)
					}
				}).catch(() => resolve(undefined))
			} else {
				resolve(undefined)
			}
		})
	}

	renderFullPage(html, initialState, head) {
		return template({
			html,
			initialState: JSON.stringify(initialState),
			helmetMeta: head.meta.toString(),
			title: head.title.toString(),
			htmlAttrs: head.htmlAttributes.toString(),
			links: head.link.toString(),
			script: head.script.toString()
		})
	}

	componentToHtml(store, props, initialData) {
		var createElementFn = serverProps => (Component, props) => (<Component {...serverProps} {...props} />)
		try {
			var html = ReactDOMServer.renderToString(
				<Provider {...store}>
					<RouterContext {...props} createElement={createElementFn({ initialData })} />
				</Provider>
			)
		} catch (err) {
			console.log(err)
		}

		var head = Helmet.rewind()
		return { html, head }
	}

	start() {

		var app = express()
		var history = createMemoryHistory()
		var routes = createRoutes(history)
		var store = getStore(initialState)

		app.use(cookieParser())
		app.use(bodyParser.json())
		app.use('/assets', express.static('dist'))
		global.navigator = { userAgent: 'all' }

		app.get('*', (req, res) => {
			match({ routes, location: req.url }, (error, redirect, props) => {
				axios.defaults.baseURL = "http://0.0.0.0:9001" //`${req.protocol}://${req.get('Host')}`

				
				if (error) {
					res.status(500).send(error.message)
					return
				}

				if (redirect) {
					res.redirect(302, `${redirect.pathname}${redirect.search}`)
					return
				}

				

				if (props) {
					var token = req.cookies.jwt
					var appJsx = props.routes[1].component
					var appGetInitialData = appJsx.getInitialData()
					var asyncComp = props.routes[props.routes.length - 1].component()

					if (asyncComp) {
						asyncComp.getInitialData().requests().then(comp => {
							asyncComp.getInitialData().fillData(comp)

							var getInitialData = comp.default.getInitialData ? comp.default.getInitialData() : { requests: () => { return [] }, fillData: () => { } }
							axios.all(appGetInitialData.requests("ru").concat(getInitialData.requests(props, token)))
								.then(response => {
									console.log("\n\n response \n\n", response)
									var defaultInitialData = { host: `${req.protocol}://${req.get('Host')}` }
									var defaultActions = {
										defaultMethod: appGetInitialData.fillData,
										defaultData: response,
										componentFillData: getInitialData.fillData
									}
									var { html, head } = this.componentToHtml(store, props, _.assign({}, defaultInitialData, defaultActions))
									res.status(200).send(this.renderFullPage(html, _.assign({}, initialState, defaultInitialData), head))
								})
								.catch(err => res.status(404).send(err))
						})
					} else {
						res.status(404).send(">>> ERROR:No component found")
					}
				} else {
					res.status(404).send('Not found')
				}
			})
		})

		app.listen(this.port)
	}
}

var server = new Server(process.env.PORT || 9000)
server.start()