import React from 'react';
import MainView from 'views/MainView';
import HeaderBlock from 'views/HeaderBlock';

import Nav from 'components/Nav';
import Footer from 'components/Footer';
import Breadcrumbs from 'components/Breadcrumbs';

import "styles/main"
export default class App extends React.Component {
	state = {
		showBreadcrumbs: true
	}

	componentDidMount() {
		this.showBreadcrumbs(this.props);
	}

	componentWillReceiveProps(nextProps) {
		this.showBreadcrumbs(nextProps);
	}

	showBreadcrumbs = (props) => {
		let { showBreadcrumbs } = this.state;
		switch(props.location.pathname) {
			case '/blog-list':
				showBreadcrumbs = false;
				break;
			default:
				showBreadcrumbs = true;
				break;
		}
		showBreadcrumbs !== this.state.showBreadcrumbs && this.setState({ showBreadcrumbs });
	}

	render() {
		let { showBreadcrumbs } = this.state;
		return (
			<div className="app_root">
				<Nav {...this.props}/>		
				{showBreadcrumbs && (
					<Breadcrumbs />
				)}
				{/* <nav className="main_menu"></nav>
				<HeaderBlock />
				<MainView /> */}
				{this.props.children}
				<Footer />
			</div>
		)
	}
}