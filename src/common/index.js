export function disableScroll(f) {
    f ? require('disable-scroll')['on']() : require('disable-scroll')['off']()
}