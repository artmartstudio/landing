import React from "react";
import { Router, Route, IndexRoute } from "react-router";
import { syncHistoryWithStore } from "mobx-react-router";

import App from "App";
import BlogItem from 'views/Blog/BlogItem';
import BlogList from 'views/Blog/BlogList';

import mainStore from 'models/MainStore';

export var initialData = mainStore

export function getStore(data) {
  return data
}

export function createRoutes(history, store) {
  if (store) {
    history = syncHistoryWithStore(history, store)
  }

  var onUpdate = () => {
    window.scrollTo(0, 0)
  }

  return (
    <Router history={history} onUpdate={onUpdate}>
      <Route path="/" component={App}>
	  	<Route path="blog-item" component={BlogItem}>
        <Route path="news" component={BlogItem} />
        <Route path="services" component={BlogItem} />
        <Route path="portfolio" component={BlogItem} />
        <Route path="contacts" component={BlogItem} />
		  </Route>
		<Route path="blog-list" component={BlogList}/></Route>
    </Router>
  )
}
