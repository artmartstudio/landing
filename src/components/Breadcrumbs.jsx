import React from 'react';
import { Link } from 'react-router';

import 'styles/breadcrumbs';

export default class Breadcrumbs extends React.PureComponent {
	breadcrumbs = [
		{
			url: '/',
			title: 'Главная'
		},
		{
			url: '/',
			title: 'Новости'
		},
		{
			url: '/',
			title: 'Новшества серверного рендеринга в React 16'
		},				
	];
	render() {
		return(
			<div className="main_breadcrumbs_wrapper">
				<ul className="main_breadcrumbs_list">
					{this.breadcrumbs.map((item, i) => {
						return(
							<Link 
								to={item.url}
								className="breadcrumbs_item"
								key={i}>
								{item.title}
							</Link>
						);
					})}
				</ul>
			</div>
		);
	}
}