import React from 'react';
import { Link } from 'react-router';

import 'styles/nav';

export default class Nav extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			currentLocation: props.location.pathname,
			menuHidden: true,
		}
	}
	componentWillReceiveProps(nextProps) {
		this.setState({ currentLocation: nextProps.location.pathname, menuHidden: true })
	}

	navList = {
		service_links: [
			{
				url: '/blog-item/services',
				title: 'Услуги'
			},
			{
				url: '/blog-item/portfolio',
				title: 'Портфолио'
			},
			{
				url: '/blog-item/contacts',
				title: 'Контакты'
			},						
		],
		blog_links: [
			{
				url: '/blog-list',
				title: 'Статьи'
			},
			{
				url: '/blog-item/development',
				title: 'Разработка'
			},
			{
				url: '/blog-item/attendance',
				title: 'Сервисы'
			},	
			{
				url: '/blog-item/news',
				title: 'Новости'
			},					
		]
	}

	toggleMenu = () => {
		this.setState({ menuHidden: !this.state.menuHidden });
	}

	render() {
		let { currentLocation, menuHidden } = this.state;
		return(
			<nav className="main_nav">
				<Link 
					to='/'
					className="nav_logo">
					Artmart <b>Studio</b>
				</Link>
				<div 
					onClick={this.toggleMenu}
					className={`toggle_menu_btn ${!menuHidden ? 'active_btn' : ''}`}>
					<div className="menu_btn_decor"></div>
					<div className="menu_btn_decor"></div>
					<div className="menu_btn_decor"></div>
				</div>
				<ul className={`nav_list ${menuHidden ? 'hidden' : ''}`}>
					{Object.keys(this.navList).map((item, i) => {
						return(
							<div 
								className={`${item} ${this.navList[item].find(el => el.url === currentLocation) ? 'active' : ''}`} //service_links //blog_links
								key={i}>
								{this.navList[item].map((el, key) => {
									return(
										<Link 
											to={el.url}
											key={key}
											className={`nav_list_link ${currentLocation === el.url ? 'active' : ''}`}>
											{el.title}
										</Link>
									);
								})}
							</div>
						);
					})}
				</ul>
			</nav>
		);
	}
}