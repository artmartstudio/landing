import React from 'react';

import 'styles/universal/main-input.sass';

export default class MainInput extends React.PureComponent {
	render() {
		return(
			<label className="main_input_label">
				{this.props.label || ''}
				<input 
					name={this.props.name || null}
					type={this.props.type || 'text'}
					placeholder={this.props.placeholder} 
					className="main_input"/>
			</label>
		);
	}
}