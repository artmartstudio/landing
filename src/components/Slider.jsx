import React from 'react';
import { Link } from 'react-router';
import Swipe from 'react-swipeable';

import 'styles/slider';
import arrow from 'images/arrow.svg';
import arrow1 from 'images/arrow1.svg';

export default class Slider extends React.PureComponent {
	constructor(props) {
		super(props);
		this.state = {
			currentSlide: props.currentSlide || 0,
		}
	}

	changeSlide = (e, direction = 'prev') => {
		e.stopPropagation();
		let { slides } = this.props;
		let { currentSlide } = this.state;

		if (direction === 'next') {
			currentSlide = currentSlide !== slides.length - 1 ? currentSlide += 1 : 0;
		} else {
			currentSlide = currentSlide !== 0 ? currentSlide - 1 : slides.length - 1; 
		}
		this.setState({ currentSlide });
	}

	swipeSlide = (direction = 'prev') => {
		let { slides } = this.props;
		let { currentSlide } = this.state;

		if (direction === 'next') {
			currentSlide = currentSlide !== slides.length - 1 ? currentSlide += 1 : 0;
		} else {
			currentSlide = currentSlide !== 0 ? currentSlide - 1 : slides.length - 1; 
		}
		this.setState({ currentSlide });
	}

	render() {
		let { slides, overlay, fullSize } = this.props;
		let { currentSlide } = this.state;
		return(
			<div 
				className={`slider_container ${slides && slides.length === 1 ? 'no_controllers' : ''}`}>
				<Swipe 
					onSwipedLeft={() => this.swipeSlide('prev')}
					onSwipedRight={() => this.swipeSlide('next')}>
					{slides && slides.length !== 1 && (
						<div 
							onClick={this.changeSlide}
							className="slider_controller prev" 
							dangerouslySetInnerHTML={{ __html: arrow1 }}>
						</div>
					)}
					{slides && slides.map((item, i) => {
						return(
							<div 
								style={{ opacity: i !== currentSlide ? 0 : 1, backgroundImage: `url(${fullSize && item.url})` }}
								key={i}
								className="slider_item">
								{!fullSize 
									&&(
										<img className="slider_item_main_image" src={item.url} alt="alt"/>
									)
								}							
							</div>
						);
					})}
					{fullSize && (
						<div 
							key={currentSlide}
							className="overlay">
							<div className="slider_article_descr">
								<div 
									style={{ backgroundImage: `url(${slides[currentSlide].article.author_avatar})` }}
									className="author_avatar">
								</div>
								<Link 
									to="/"
									className="slider_title">
									{slides[currentSlide].article.title}
								</Link>
								<div className="article_descr">
									<div className="badge">{slides[currentSlide].article.badge}</div>
									<div className="article_date">
										<div className="date">{slides[currentSlide].article.article_date}</div>
										<Link
											to="/"
											className="author_name">
											{slides[currentSlide].article.author}
										</Link>
									</div>
								</div>
							</div>
						</div>
					)}				
					{slides && slides.length !== 1 && (
						<div 
							onClick={(e) => this.changeSlide(e,'next')}
							className="slider_controller next" 
							dangerouslySetInnerHTML={{ __html: arrow1 }}>
						</div>
					)}
				</Swipe>
			</div>
		);
	}
}