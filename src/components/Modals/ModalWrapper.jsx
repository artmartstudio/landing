import React from 'react';
import { disableScroll } from 'common';

import 'styles/main';
import cross from 'images/cross.svg';

export default class ModalWrapper extends React.PureComponent {
	closeModal = () => {
		this.props.onToggle && this.props.onToggle()
	}

	handleWheel = (e) => {
		e.stopPropagation();
		e.nativeEvent.stopImmediatePropagation();
	}

	componentDidMount() {
		if (!/Mac|iPad|iPhone/.test(navigator.platform) && !this.props.noPreventScroll) {
			disableScroll(true);
		}		
	}

	componentWillUnmount() { 
		if (!/Mac|iPad|iPhone/.test(navigator.platform) && !this.props.noPreventScroll) {
			disableScroll(false);
		}	
	}	

	render() {
		let { closeBtn } = this.props;
		return(
			<div 
				onClick={this.closeModal}
				onWheel={this.handleWheel}
				className={`main_modal_wrapper`}>
				{closeBtn && (
					<button 
						onClick={this.closeModal}
						dangerouslySetInnerHTML={{ __html: cross }}
						className="close_btn">						
					</button>
				)}
				{this.props.children}
			</div>
		);
	}
}