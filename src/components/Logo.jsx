import React from 'react';
import { Link } from 'react-router';

import 'styles/logo.sass';

export default class Footer extends React.PureComponent {

	render() {
		return (
			<Link target="blank" rel="nofollow" to="http://artmartstudio.net" className="footer_logo">
				<div className="footer_logo_decor_border"></div>
				<div className="footer_logo_decor_border"></div>
				<div className="footer_logo_decor_border"></div>
				<div className="footer_logo_decor_border"></div>
				<span className="logo_name">A M</span> 
				<div className="footer_logo-decor">studio</div>
			</Link>
		);
	}
}