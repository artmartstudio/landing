import React from 'react';
import { Link } from 'react-router';

import Logo from './Logo';
import 'styles/footer';

import fb from 'images/fb1.svg';
import codepen from 'images/codepen.svg';
import dribble from 'images/dribble.svg';
import git from 'images/git.svg';

export default class Footer extends React.PureComponent {
	socials = [
		{
			name: 'git',
			url: '/',
			img: git
		},
		{
			name: 'dribble',
			url: '/',
			img: dribble	
		},	
		{
			name: 'codepen',
			url: '/',
			img: codepen	
		},
		{
			name: 'fb',
			url: '/',
			img: fb	
		}						
	];

	render() {
		return(
			<footer 
				className="main_footer">
				<div className="contacts_block">
					<div className="contacts_items">
						<Link
							to="/" 
							className="contacts_item_value">Идеология</Link>
						<Link
							to="/" 
							className="contacts_item_value">Сотрудничество</Link>
						<Link
							to="/" 
							className="contacts_item_value">Вакансии</Link>
					</div>
					<div className="contacts_items">
						<Link
							to="/"
							className="contacts_item_value">Украина, Киев, ул. Рогнединская 3</Link>
						<a className="contacts_item_value phone" href="tel:+380 (93) 529 38 38">+380 (93) 529 38 38</a>
						{/* <span className="">+380 (93) 529 38 38</span> */}
						<a className="contacts_item_value mail" href="mailto:mail@artmart.studio">mail@artmart.studio</a>
						{/* <span className="contacts_item_value mail"></span> */}
					</div>
				</div>
				<div className="socials_block">
					{this.socials.map((item, i) => {
						return(
							<Link 
								to={item.url}
								key={i}
								dangerouslySetInnerHTML={{ __html: item.img }}
								className={`socials_block_item ${item.name}`}>
							</Link>
						);
					})}					
				</div>
				<Logo />
			</footer>
		);
	}
}