import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react'
import FastClick from 'react-fastclick-alt';
import { browserHistory } from 'react-router'
import { createRoutes } from 'routes';
import store from 'models/MainStore'

ReactDOM.render(
    <Provider {...store}>
        <FastClick>
            { createRoutes(browserHistory, store.routing) }
        </FastClick>
    </Provider>,

    document.getElementById('app')
)