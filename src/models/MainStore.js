import { observable } from 'mobx'
import { RouterStore } from "mobx-react-router"

const routingStore = new RouterStore()

const store = {
	routing: routingStore
}

export default store