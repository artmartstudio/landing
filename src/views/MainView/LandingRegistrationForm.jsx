import React from 'react';
import MainInput from 'components/Form/MainInput';

import 'styles/mp-landings/form.sass';

export default class LandingRegistrationForm extends React.PureComponent {
	render() {
		let { login, form } = this.props;

		return(
			<form className="form_wrapper" onSubmit={(e) => e.preventDefault()}>
				<h3 className="form_title">{login ? form.loginTitle : form.registrTitle}</h3>
				<MainInput label={form.nameLabel} placeholder={form.namePlaceholder}/>

				{!login && (
					<MainInput label={form.emailLabel} placeholder={form.emailPlaceholder}/>
				)}

				<MainInput label={form.pwdLabel} type="password" placeholder={form.passwordPlaceholder}/>
				
				<button className="main_button form_submit_btn">
					{login ? form.loginBtnTitle : form.registrBtnTitle}
				</button>
			</form>
		);
	}
}