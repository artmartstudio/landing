import React from 'react';

import LandingRegistrationForm from './LandingRegistrationForm';

import 'styles/mp-landings/mp-landings.sass';

export default class MPLandings extends React.PureComponent {
	state={
		animate: false
	}
	form={
		loginTitle: 'Wellcome back,',
		registrTitle: 'Time to feel like home',
		namePlaceholder: 'enter name',
		passwordPlaceholder: 'enter pwd',
		emailPlaceholder: 'enter mail',
		loginBtnTitle: 'sign up',
		registrBtnTitle: 'sign in',	
		nameLabel: 'name',
		pwdLabel: 'password',
		emailLabel: 'e-mail',	
	}
	triggerAnimation = () => {
		this.setState({ animate: !this.state.animate });
	}
	render() {
		let { animate } = this.state;
		return(
			<div className={`login_form_wrapper`}>
				<div className={`opacity_block ${animate ? 'to_left' : 'to_right'}`}>
					<div className="titles_wrapper">
						<div className="title_block title_left">
							<h2 className="title">Hola!</h2>
							<p className="title_descr">
								Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ratione, ut?
							</p>
						</div>
						<div className="title_block title_right">
							<h2 className="title">Wilkomen!</h2>
							<p className="title_descr">
								Lorem ipsum dolor sit, amet consectetur adipisicing elit. Ratione, ut?
							</p>	
						</div>
					</div>													
					<button 
						onClick={this.triggerAnimation}
						className="change_form main_button">press</button>									
					<div className="opacity_white left">
						<LandingRegistrationForm form={this.form} login/>
					</div>
					<div className="opacity_white right">
						<LandingRegistrationForm form={this.form}/>				
					</div>

					<div className="disappearing_image left_img"></div>	
					<div className="disappearing_image right_img"></div>
				</div>
			</div>
		);
	}
}