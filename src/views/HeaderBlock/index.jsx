import React from 'react';
import { easeOutBack } from 'easing-utils';

import 'styles/header/header-block';

export default class HeaderBlock extends React.PureComponent {
	constructor(props) {
		super(props);

		this.duration = 3000;
		this.currentStep = 1;
		this.currentRotate = 45;
		this.clockWise = -1;
		this.framesCount = this.duration *60  / 1000;
		this.rotateOn = (45 / this.framesCount);
		this.translateOn = (450 / this.framesCount *3);
	}

	componentDidMount() {
		this.animate();
	}

	normalize(value, min, max, unnormMax) {
		var fmax = max - min;
		var multiplier = fmax / unnormMax;
		return value * multiplier + min;
	} 

	timingFunction = (t) => {
		// if (t <= 1 && t >= 0) {
			return easeOutBack(t) 
		// }
	}
	
	animate = () => {
		let start = Date.now();
		let container = this.refs.container;
		requestAnimationFrame(			
			this.step = (timestamp) => {
				let durr = Date.now();
				let progress = ((durr - start) *60) /1000;	
				let translated = this.translateOn * this.currentStep <= 450 ? this.translateOn * this.currentStep : 450;
				if (progress <= this.framesCount || progress >= this.framesCount && this.currentRotate !== 0) {
					this.changeCurrentRotate();			
					this.currentStep = Math.ceil(progress);	
					container.style.transform = `translateY(${translated}px) rotate(${this.currentRotate * this.timingFunction((durr - start) / this.duration)}deg)`;	
					requestAnimationFrame(this.step);					
				}
			}
		)
		this.step();
	}

	changeCurrentRotate = () => {
		if (this.currentRotate >= 45) {
			this.clockWise = -1;
		} else if(this.currentRotate <= -45) {
			this.clockWise = 1;
		}
		this.currentRotate += this.clockWise * this.rotateOn *4 ;
	}

	render() {
		return(
			<header className="header_block">
				<h1 className="header_main">
					<span title="Продать"><span>Продать</span></span>
					<span title="Квартиру"><span>Квартиру</span></span>
					<span title="Киев" className="city_name"><span >Киев</span></span>						
				</h1>
				<div className="btn_container" ref="container">
					<div className="cable"></div>
					<button 
						className="submit_advertisement">
						подать объявление
					</button>
				</div>							
			</header>
		);
	}
}