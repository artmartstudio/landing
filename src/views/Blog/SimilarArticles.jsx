import React from 'react';
import SimilarArticlesItem from './SimilarArticlesItem';

import { Link } from 'react-router';

import 'styles/blog/similar-articles';

export default class SimilarArticles extends React.PureComponent {
	render() {
		return(
			<div className="silimar_articles_container">
				<h2 className="similar_articles_title">
					Похожие статьи
				</h2>
				
				<div className="silimar_articles_wrapper">
					<SimilarArticlesItem photo="https://i.ytimg.com/vi/zHn2pXDZH6k/maxresdefault.jpg"/>
					<SimilarArticlesItem />
					<SimilarArticlesItem />
					<SimilarArticlesItem />
					<SimilarArticlesItem />
				</div>				
			</div>
		);
	}
}