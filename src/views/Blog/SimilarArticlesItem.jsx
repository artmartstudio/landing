import React from 'react';
import { Link } from 'react-router';

import 'styles/blog/similar-articles-item';

export default class SimilarArticlesItem extends React.PureComponent {
	render() {
		let { coverItem } = this.props;
		return(
			<Link 
				to="/123"
				className={`silimar_articles_item ${coverItem ? 'cover_item' : ''}`}>
				<div
					//style={{ backgroundImage: `url(${this.props.photo || 'https://images4.alphacoders.com/432/43258.jpg'})` }} 
					className="silimar_cover_img">
					<div className="img_wrapper">
						<div 
							style={{ backgroundImage: `url(${this.props.photo || 'https://images4.alphacoders.com/432/43258.jpg'})` }}
							className="img_block">
						</div>
						{coverItem 
							?(
								<div className="article_descr">
									<h3 className="article_descr_title">
										Lorem ipsum, dolor sit amet consectetur adipisicing.
									</h3>	
									<div className="article_date_badge">
										<div className="type_badge">Новости</div>
										<div className="similar_descr">
											<div className="similar_date">14 сентября 2017</div>
											<Link 
												to="/"
												className="similar_author">John Doe</Link>
										</div>
									</div>						
								</div>
							) 
							:(
								<div className="silimar_type_badge">
									Новости
								</div>
							)
						} 					
					</div>
				</div>
				{!coverItem && (
					<div className="similar_descr_block_wrapper">
						<h3 className="silimar_title">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Earum, eaque.</h3>
						<div className="similar_descr">
							<div className="similar_date">14 сентября 2017</div>
							<Link 
								to="/"
								className="similar_author">John Doe</Link>
						</div>
					</div>
				)}
			</Link>			
		);
	}
}