import React from 'react';
import Slider from 'components/Slider';
import SimilarArticlesItem from './SimilarArticlesItem';

import 'styles/blog/blog-list';
// import bg from 'images/telegram-bg.svg';

export default class BlogList extends React.PureComponent {
	images = [
		{
			url: `https://images4.alphacoders.com/432/43258.jpg`,
			article: {
				author: 'John Doe',
				author_avatar: 'http://storage.proboards.com/3217245/avatar/Co0EMcp15z0FZ4zeUvXz.png',
				title: 'Далеко-далеко за словесными горами, в стране гласных и согласных живут рыбные тексты.',
				article_date: '14 september 2017',
				badge: 'News',
			}
		},
		{
			url: `http://www.intrawallpaper.com/static/images/Golden-Gate-Bridge-HD-Wallpapers-WideScreen_FK1cfem.jpg`,
			article: {
				author: 'John Doe',
				author_avatar: 'http://storage.proboards.com/3217245/avatar/Co0EMcp15z0FZ4zeUvXz.png',
				title: 'Lorem ipsum, Далеко-далеко за словесными горами, в стране гласных и согласных живут рыбные тексты.',
				article_date: '14 september 2017',
				badge: 'News',
			}
		},
		{
			url: `http://www.intrawallpaper.com/static/images/new_wallpaper_nature_hd_2015-1.jpg`,
			article: {
				author: 'John Doe',
				author_avatar: 'http://storage.proboards.com/3217245/avatar/Co0EMcp15z0FZ4zeUvXz.png',
				title: 'Далеко-далеко за словесными горами, в стране гласных и согласных живут рыбные тексты. lor sit amet consectetur adipisicing elit. A, delectus.',
				article_date: '14 september 2017',
				badge: 'News',
			}
		},
	];	

	state = {
		mobileScreen: false
	}
	
	componentDidMount() {
		var mobileScreen = window.matchMedia("(max-width: 729px)").matches;
		this.setState({ mobileScreen })
	}
	render() {
		let { mobileScreen } = this.state;
		console.log(mobileScreen)
		return(
			<section className="bloglist">
				<Slider fullSize slides={this.images} />
				<div className="similar_articles">
					<div className="articles">
						{/* <SimilarArticlesItem /> */}
						<SimilarArticlesItem />
						<SimilarArticlesItem />
					</div>
				</div>
				<main className="all_articles">
					<div className="articles">
						{[...Array(7)].map((item, i) => {
							return(
								<SimilarArticlesItem coverItem={i === 0 && !mobileScreen} key={i}/>
							);
						})}
					</div>
					<button className="more_articles blog_button_blue">
						Еще статьи
					</button>
				</main>
				<div className="similar_articles">
					<div className="articles">
						{/* <SimilarArticlesItem /> */}
						<SimilarArticlesItem />
						<SimilarArticlesItem />
					</div>
				</div>
				<div className="subscribe_section">
					<h2 className="subscribe_title">Telegram-канал</h2>
					<p className="subscribe_descr">Читайте статьи, следите за нашими разработками</p>
					<div className="subscribe_btn_block">
						<a className="subscribe_link" href="/">t.me/artmartstudio</a>
						<button className="blog_button_white subscribe_btn">Подписаться</button>
					</div>
				</div>				
			</section>
		);
	}
}
