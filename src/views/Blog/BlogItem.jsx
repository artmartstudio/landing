import React from 'react';
import { Link } from 'react-router';

import ModalWrapper from 'components/Modals/ModalWrapper';
import Slider from 'components/Slider';
import SimilarArticles from './SimilarArticles';

import 'styles/blog/blog-item';

import gplus from 'images/gplus.svg';
import fb from 'images/fb.svg';
import quote from 'images/quoteBraces.svg';

export default class BlogItem extends React.PureComponent {
	state = {
		currentSlide: null,
	}
	images = [
		{
			url: `https://images4.alphacoders.com/432/43258.jpg`,
		},
		{
			url: `http://www.intrawallpaper.com/static/images/Golden-Gate-Bridge-HD-Wallpapers-WideScreen_FK1cfem.jpg`,
		},
		{
			url: `http://www.intrawallpaper.com/static/images/new_wallpaper_nature_hd_2015-1.jpg`,
		},
		{
			url: `https://i.ytimg.com/vi/zHn2pXDZH6k/maxresdefault.jpg`,
		},
		{
			url: `https://i.ytimg.com/vi/eXUsE1MCBUs/maxresdefault.jpg`,
		},								
	];
	coverImage = [
		{
			url: `http://mymoscowcity.com/upload/iblock/a9c/a9c2b0b174448e6054cbbbd957fd0429.jpg`,
		}
	];
	openSlider = (item, array) => {
		this.setState({ currentSlide: item, slides: array })
	}
	render() {
		let { currentSlide, slides } = this.state;
		return (
			<section className="blog_item_container">
				{currentSlide !== null && (
					<ModalWrapper
						noPreventScroll
						closeBtn 
						onToggle={() => this.setState({ currentSlide: null })}>
						{ typeof currentSlide === 'string' 
							?(
								<div 
									style={{ backgroundImage: `url(${currentSlide})` }}
									className="full_size_img">									
								</div>
							) 
							:(
								<Slider currentSlide={currentSlide} slides={slides} /> 
							) 
						}
					</ModalWrapper>
				)}
				<article 
					className="blog_item">
					<h1 
						className="blog_item_title">
						Lorem ipsum dolor sit amet consectetur adipisicing elit. Dolores magnam et commodi veniam perferendis iure?
					</h1>
					<div className="article_info">
						<div className="article_author">
							<Link 
								to="/"
								style={{ backgroundImage: `url(https://vignette2.wikia.nocookie.net/shre/images/c/cc/Shrek_smiling.jpg/revision/latest?cb=20170719161246&path-prefix=ru)` }}
								className="author_avatar">
							</Link>
								<div className="author_descr">
									<Link 
										to="/"
										className="author_name">
										John Kek
									</Link>
									<div className="date">14 сентября 2017</div>
								</div>
						</div>
						<div className="article_socials">
							<div className="socials_item">
								<div 
									className="social_img gplus" 
									dangerouslySetInnerHTML={{ __html: gplus }}>
								</div>
								<p className="social_descr">12 google+</p>
							</div>
							<div className="socials_item">
								<div 
									className="social_img fb" 
									dangerouslySetInnerHTML={{ __html: fb }}>
								</div>
								<p className="social_descr">10 shares</p>
							</div>							
						</div>
					</div>

					<main className="article_content">
						<div className="article_images_block">
							{this.coverImage.map((item, i, arr) => {
								return(
									<div 
										style={{ width: arr.length === 1 && `${100}%` }}
										className="wrapper"
										key={i}>
										<div 
											key={i}
											onClick={ () => this.openSlider(i, arr) }
											style={{ backgroundImage: `url(${item.url})` }}
											className="images_block_item">
										</div>
									</div>
								);
							})}
						</div>					

						<h2 className="article_h2">Lorem ipsum dolor sit amet.</h2>
						<p className="article_paragraph">
							Lorem, ipsum dolor sit amet consectetur adipisicing elit. Itaque illo quae laboriosam nostrum consectetur doloremque iusto saepe. Praesentium perspiciatis fugiat quibusdam veniam ullam enim beatae. Doloremque voluptatibus in iusto odit ut quasi sunt quas velit consequuntur?
						</p>
						<blockquote 
							className="article_quote">
							<div className="quote_decor" dangerouslySetInnerHTML={{__html: quote }}></div>
							<p className="quote_descr">Lorem ipsum dolor sit amet consectetur adipisicing elit. Esse natus quod accusantium ipsum suscipit incidunt necessitatibus tempora nemo sed nam.</p>
							<cite className="quote_author"></cite>
						</blockquote>	
						<p className="article_paragraph">
							Lorem, ipsum dolor sit amet consectetur adipisicing elit. Itaque illo quae laboriosam nostrum consectetur doloremque iusto saepe. Praesentium perspiciatis fugiat quibusdam veniam ullam enim beatae. Doloremque voluptatibus in iusto odit ut quasi sunt quas velit consequuntur? Ratione quia rerum laudantium libero iste nulla, adipisci fugiat omnis alias quaerat est fugit animi assumenda, deleniti autem, beatae consectetur dignissimos odit vitae deserunt nobis corrupti voluptas possimus? Earum inventore velit voluptas molestiae, facilis vel quis voluptates, magni libero soluta facere hic similique? Ducimus harum aspernatur enim tempora mollitia quos, sapiente maiores sint libero dolores odio temporibus quasi sed ratione id reprehenderit eos saepe?
						</p>						
						<h3 
							className="article_h3">
							Lorem ipsum dolor sit amet.
						</h3>
						<p className="article_paragraph">
							Lorem, ipsum dolor sit amet consectetur adipisicing elit. Itaque illo quae laboriosam nostrum consectetur doloremque iusto saepe. Praesentium perspiciatis fugiat quibusdam veniam ullam enim beatae. Doloremque voluptatibus in iusto odit ut quasi sunt quas velit consequuntur? Ratione quia rerum laudantium libero iste nulla, adipisci fugiat omnis alias quaerat est fugit animi assumenda, deleniti autem, beatae consectetur dignissimos odit vitae deserunt nobis corrupti voluptas possimus? Earum inventore velit voluptas molestiae, facilis vel quis voluptates, magni libero soluta facere hic similique? Ducimus harum aspernatur enim tempora mollitia quos, sapiente maiores sint libero dolores odio temporibus quasi sed ratione id reprehenderit eos saepe?
						</p>						
						<h4 className="article_h4">Lorem ipsum dolor sit amet.</h4>
						<p className="article_paragraph">
							Lorem, ipsum dolor sit amet consectetur adipisicing elit. Itaque illo quae laboriosam nostrum consectetur doloremque iusto saepe. Praesentium perspiciatis fugiat quibusdam veniam ullam enim beatae. Doloremque voluptatibus in iusto odit ut quasi sunt quas velit consequuntur? Ratione quia rerum laudantium libero iste nulla, adipisci fugiat omnis alias quaerat est fugit animi assumenda, deleniti autem, beatae consectetur dignissimos odit vitae deserunt nobis corrupti voluptas possimus? Earum inventore velit voluptas molestiae, facilis vel quis voluptates, magni libero soluta facere hic similique? Ducimus harum aspernatur enim tempora mollitia quos, sapiente maiores sint libero dolores odio temporibus quasi sed ratione id reprehenderit eos saepe?
						</p>						
						<div className="article_images_block">
							{this.images.map((item, i, arr) => {
								return(
									<div 
										style={{ width: arr.length === 1 && `${100}%` }}
										className="wrapper"
										key={i}>
										<div 
											key={i}
											onClick={ () => this.openSlider(i, arr) }
											style={{ backgroundImage: `url(${item.url})` }}
											className="images_block_item">
										</div>
									</div>
								);
							})}
						</div>
						<p className="article_paragraph">
							Lorem, ipsum dolor sit amet consectetur adipisicing elit. Itaque illo quae laboriosam nostrum consectetur doloremque iusto saepe. Praesentium perspiciatis fugiat quibusdam veniam ullam enim beatae. Doloremque voluptatibus in iusto odit ut quasi sunt quas velit consequuntur? Ratione quia rerum laudantium libero iste nulla, adipisci fugiat omnis alias quaerat est fugit animi assumenda, deleniti autem, beatae consectetur dignissimos odit vitae deserunt nobis corrupti voluptas possimus? Earum inventore velit voluptas molestiae, facilis vel quis voluptates, magni libero soluta facere hic similique? Ducimus harum aspernatur enim tempora mollitia quos, sapiente maiores sint libero dolores odio temporibus quasi sed ratione id reprehenderit eos saepe?
						</p>						
					</main>					
				</article>
				<SimilarArticles />				
			</section>
		);
	}
}