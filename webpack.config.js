var path = require('path');
var webpack = require('webpack');
var autoprefixer = require('autoprefixer');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');

process.noDeprecation = true;

var config = {
	entry: {
		app: path.resolve(__dirname, 'src/index.js')
	},
	output: {
		path: path.resolve(__dirname, "dist/"),
		sourceMapFilename: '[file].map',
		publicPath: '/assets/',
		filename: 'bundle.[hash].js'
	},
	module: {
		loaders: [
			{
				test: /\.(jsx|js)$/,
				loader: 'babel-loader',
				// exclude: /node_modules/,
				query: {
					presets: ["es2015", "stage-0", "react"],
					plugins: ["transform-decorators-legacy"]
				}
			},

			{
				test: /\.(css)$/,
				loader: ExtractTextPlugin.extract({ fallback: "style-loader", use: "css-loader!postcss-loader" })
			},

			{
				test: /\.(scss|sass)$/,
				loader: ExtractTextPlugin.extract({ fallback: "style-loader", use: "css-loader!postcss-loader!sass-loader" })
			},

			{
				test: /\.svg$/,
				include: [path.resolve(__dirname, 'src', 'images')],
				exclude: [path.resolve(__dirname, 'src', 'images', 'dist')],
				loader: 'svg-inline-loader'
			},

			{
				test: /\.(svg|woff|woff2|ttf|png)$/,
				include: [
					path.resolve(__dirname, 'src', 'images', 'dist'),
					path.resolve(__dirname, 'src', 'fonts')
				],
				loader: 'url-loader',
				query: {
					limit: 1000,
					name: '[hash].[ext]'
				}
			},

			{
				test: /\.json$/,
				loader: 'json-loader'
			}
		]
	},

	resolve: {
		modules: [path.resolve(__dirname, "src"), "node_modules"],
		extensions: [".js", ".jsx", ".scss", ".sass", ".json", ".css"]
	},

	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: JSON.stringify('production')
			}
		}),

		new webpack.optimize.UglifyJsPlugin(),

		new ExtractTextPlugin('[hash].css'),

		new HtmlWebpackPlugin({
			template: 'index.ejs',
			filename: 'index.tmpl.html',
			html: '${this.html}',
			extraScript: '${this.initialState}',
			title: '${this.title}',
			helmetMeta: '${this.helmetMeta}'
		}),

		new webpack.DefinePlugin({ "global.GENTLY": false }),

		new webpack.optimize.OccurrenceOrderPlugin(),
	]
};

module.exports = config;