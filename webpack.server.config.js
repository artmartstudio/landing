var path = require('path')
var webpack = require('webpack')

var config = {
	entry: path.resolve(__dirname, 'server.js'),
	target: 'node',
	output: {
		path: __dirname,
		filename: 'server.bundle.js',
		libraryTarget: 'commonjs2'
	},
	module: {
		loaders: [
			{
				test: /\.(jsx|js)$/,
				loader: 'babel-loader',
				// exclude: /node_modules/,
				query: {
					presets: ["es2015", "stage-0", "react"],
					plugins: ["transform-decorators-legacy", "transform-class-properties"]
				}
			},
			{
				test: /\.(scss|sass|css)$/,
				loader: 'ignore-loader'
			},
			{
				test: /\.svg$/,
				include: [path.resolve(__dirname, 'src', 'images')],
				exclude: [path.resolve(__dirname, 'src', 'images', 'dist')],
				loader: 'svg-inline-loader'
			},
			{
				test: /\.(svg|woff|woff2|ttf|png)$/,
				include: [
					path.resolve(__dirname, 'src', 'images', 'dist'),
					path.resolve(__dirname, 'src', 'fonts')
				],
				loader: 'ignore-loader',
				query: {
					limit: 1000,
					name: '/dist/[hash].[ext]'
				}
			},
			{
				test: /\.json$/,
				loader: 'json-loader'
			},
			{
				test: /\.html$/,
				loader: 'babel-loader?presets=es2015!es6-template-string-loader'
			}
		]
	},
	resolve: {
		modules: [path.resolve(__dirname, "src"), "node_modules"],
		extensions: [".js", ".jsx", ".scss", ".sass", ".json", ".css"]
	},
	plugins: [
		new webpack.DefinePlugin({
			'process.env': {
				NODE_ENV: JSON.stringify('production')
			}
		}),

		new webpack.optimize.UglifyJsPlugin(),

		new webpack.DefinePlugin({ "global.GENTLY": false }),
	]
}

module.exports = config
